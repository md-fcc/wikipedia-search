My Wikipedia search website to fulfill the [FreeCodeCamp challenge](https://www.freecodecamp.org/challenges/build-a-wikipedia-viewer).

Live website: [Wikipedia Search](https://md-fcc.gitlab.io/wikipedia-search/)

This website uses [jQuery UI](https://jqueryui.com/) to show search suggestions obtained from the Wikipedia API as you type.

Click the dice icon to immediately go to a random Wikipedia page.