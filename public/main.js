
$(document).ready(function () {
    
    
    let prevSearch = '' // variable for checking previous search vs current search
    
    function doSearch() {
        let searchText = $("#searchInput").val();
        
        if (searchText == prevSearch && $.trim($('#results').html()).length) {
            return;
        }    
        clearResults();
        searchWikipedia(searchText);
        prevSearch = searchText;        
    }
    
    function clearResults() {
        $("#results").empty();
    }


    function searchWikipedia(searchString) {    
        let endpoint = 'https://en.wikipedia.org/w/api.php';
        let q = '?action=opensearch&format=json&origin=*&search=' + searchString;

        $.getJSON(endpoint + q, handleSearchResults);
    }


    function handleSearchResults (results) {
        // results from search are returned in an array.
        // split out results array and display on webpage
        console.log(results);

        let titles = results[1],
            descriptions = results[2],
            urls = results[3];

        for(let i = 0, len = titles.length; i < len; i++) {
            let result = 
                "<div class=\"well well-sm\">" + 
                    "<h3><a href=\"" + urls[i] + "\" target=\"_blank\">" + titles[i] + "</a></h3>" + 
                    "<p>" + descriptions[i] + "<p>" +
                "</div>";

            $("#results").append(result);
        }
    }
    
    // ------------------- Functionality for HTML Inputs -----------------------
    $("#searchInput").autocomplete({
        // provides a drop-down list of suggestions as you type. Code adapted from
        // https://w3lessons.info/2015/03/01/autocomplete-search-using-wikipedia-api-and-jquery-ui/
        // jquery-ui autocomplete documentation at:
        // http://api.jqueryui.com/autocomplete/
        source: function(request, response) {
            $.ajax({
                url: "https://en.wikipedia.org/w/api.php",
                dataType: "jsonp",
                data: {
                    'action': "opensearch",
                    'format': "json",
                    'search': request.term
                },
                success: function(data) {
                    response(data[1]);
                }
            });
        }
    });
    
    
    $('#search').on('click', (e) => {
        doSearch(); 
    });

    $('#searchInput').on('keyup', (e) => {
        if (e.keyCode == 13) {
            doSearch();
        }
    });
    
    $('#clear').on('click', (e) => {
        clearResults()
    });
})

